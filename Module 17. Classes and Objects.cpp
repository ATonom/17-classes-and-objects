﻿// Module 17. Classes and Objects.cpp 

#include <iostream>
#include <string>
#include <cmath>

std::string SetPrecision(std::string s, int precision)
{
    for (int i = 0; i <= (s.length() - 1); i++)
    {
        if (s[i] == '.')
        {
            s.resize(i + 1 + precision, '0');
            break;
        } 
    }
    return s;
}

class Point 
{
public:

    Point() : x(0), y(0), z(0)
    {}

    Point(const double& x, const double& y, const double& z): x(x), y(y), z(z)
    {}

    Point(const Point& other) 
    {
        this->x = other.x;
        this->y = other.y;
        this->z = other.z;
    }

    double GetX()
    {
        return x;
    }

    double GetY()
    {
        return y;
    }

    double GetZ()
    {
        return z;
    }

    void SetX(const double& x)
    {
        this->x = x;
    }

    void SetY(const double& y)
    {
        this->y = y;
    }

    void SetZ(const double& z)
    {
        this->z = z;
    }

    void SetСoordinates(const double& x1, const double& y1, const double& z1)
    {
        x = x1;
        y = y1;
        z = z1;
    }

    void SetСoordinates(const Point& other)
    {
        this->x = other.x;
        this->y = other.y;
        this->z = other.z;
    }

    std::string PrintСoordinates()
    {
        return "( " + SetPrecision(std::to_string(x), 2) + " , "
                    + SetPrecision(std::to_string(y), 2) + " , "
                    + SetPrecision(std::to_string(z), 2) + " )";
    }

private:
    double x, y, z;
};

class Vector
{
public:
    Vector(): x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << std::endl;
    }

    double Length()
    {
        return sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
    }

private:
    double x, y, z;
};

int main()
{
    std::cout.setf(std::ios::fixed);
    std::cout.precision(2);

    Point p1(10, 4, 3);
    std::cout 
        << "Point p1 coordinates ( " 
        << p1.GetX() << " , " 
        << p1.GetY() << " , " 
        << p1.GetZ() << " )" << std::endl;

    Point p2;
    std::cout << "Point p2 coordinates " << p2.PrintСoordinates() << std::endl;
        
    p2.SetСoordinates(5,5,5);
    std::cout << "Point p2 coordinates " << p2.PrintСoordinates() << std::endl;

    p2.SetСoordinates(p1);
    std::cout << "Point p2 coordinates " << p2.PrintСoordinates() << std::endl;

    Point p3(p1);
    std::cout << "Point p3 coordinates " << p3.PrintСoordinates() << std::endl;

    Vector v1(3, 4, 0);
    v1.Show();
    std::cout << "Vector length: " << v1.Length() << std::endl;
}

/*
* 1. Создать класс со своими данными, скрытыми при помощи private.
* Сделать public метод для вывода этих данных. Протестировать.
*
* 2. Дополнить класс Vector public методом, который будет возвращать длину (модуль) вектора.
* Протестировать.
*/